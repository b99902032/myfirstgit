public class POOBoard{
    int count;
    public String boardName;
    POOArticle[] Articles;

    public POOBoard(String name){
        this.count     = 0;
        this.boardName = name;
        this.Articles  = new POOArticle[1024];
        return;
    }

    public void add(POOArticle article){
        
        if(count == 1024)
            return;
       
        this.Articles[count] = article;
        this.count = this.count + 1;
        return;
    }

    public void del(int pos){
       
        if(pos >= count)
            return;
        this.Articles[pos].del(); 
        for(int i=pos+1;i<count;i++){
            this.Articles[i-1] = this.Articles[i]; 
        }
        this.count = this.count - 1;
        return;
    }
    
    public void move(int src, int dst){

        if(src >= count || dst >= count)
            return;
        if(src == dst)
           return;
        if(src > dst){
            int tmp = src;
            src = dst;
            dst = tmp;
        } 
        for(int i=src+1;i<=dst;i++){
            swap(i-1,i);
        }
        return;
    }
    
    public int length(){
        return count;
    }

    public void show(){
        if(count==0){
            System.out.printf("No Article Available now.\n");
            return;
        }
        System.out.printf("#Num\tAuthor\tTitle\n");
        for(int i = 0;i<count;i++){
            this.Articles[i].show(i+1);
        }
    }
    private void swap(int src, int dst){
        POOArticle tmp = this.Articles[src];
        this.Articles[src] = this.Articles[dst];
        this.Articles[dst] = tmp;
    }
    public void del_self(){
        for(int i=0;i<count;i++)
            this.Articles[i].del();
    }
}
