import java.util.Scanner;
import java.io.*;
import java.util.InputMismatchException;

public class UI{
    public String userName;
    private static Scanner scan = new Scanner(System.in);

    public int board_length;
    public POOBoard[] board;
    public int queueLen;
    public POODirectory[] DirQueue;
    
    public static int getInteger() {
        try {
            int t = scan.nextInt();
            String tmp = scan.nextLine();
            return t;
        }catch(InputMismatchException e) {
            String tmp = scan.nextLine();
            return -1;
        }
    } 
    public UI(){
        System.out.print("Please enter your name: ");
        this.userName = scan.nextLine();
        System.out.printf("Welcome, %s.\n",this.userName);
        this.queueLen = 1;
        this.board_length = 0;
        this.board = new POOBoard[1024];
        this.DirQueue = new POODirectory[40];
        this.DirQueue[0] = new POODirectory("root");
    }
    public void SelectOptionMain(){
        int res;
        while(true){
            System.out.print("\n\n\n------------=POO=BBS=-----------------\n");
            System.out.print("Please Selcet menu: \n");
            System.out.print("#1. Board Menu.\n");
            System.out.print("#2. My favorite list Menu.\n");
            System.out.print("#3. Exit POO-BBS.\n\n");
            System.out.print("Please Selcet option: ");
            res = getInteger();
            switch(res){
                case 1: selBoardOP();
                        break;
                case 2: selMyFolderOP();
                        break;
                case 3: //do noting
                        return;
            }
        }
    }
    

    private void selBoardOP(){
        int res;
        while(true){
            System.out.printf("\n\n\n------------=Board Menu=-----------------\n");
            System.out.print("#1. Create new board.\n");
            System.out.print("#2. Browse one board.\n");
            System.out.print("#3. Delete board.\n");
            System.out.print("#0. return to Top\n\n");
            System.out.print("Please Selcet option: ");
            res = getInteger();
            switch(res){
                case 1: CreateNewBoard();
                        break;
                case 2: BrowseBoard();
                        break;
                case 3: DeleteCurrentBoard();
                        break;
                case 0: //do nothing
                        return;
            }
        }
    }
    private void selMyFolderOP(){
        int res;
        POODirectory tmp = DirQueue[queueLen-1];
        while(true){
            System.out.printf("\n\n\n------------=%s=-----------------\n",tmp.name);
            System.out.print("#1. Create Entry to My favorite.\n");
            System.out.print("#2. Browse my favorite folders.\n");
            System.out.print("#3. Delete entry of My favorite.\n");
            System.out.print("#4. Modify entry order.\n");
            System.out.print("#0. return to Top\n\n");
            System.out.print("Please Selcet option: ");
            res = getInteger();
            switch(res){
                case 1: AddNewEntry();
                        break;
                case 2: selFolderMenu(tmp);
                        break;
                case 3: DeleteEntry();
                        break;
                case 4: MoveEntryLoc(tmp);
                        break;
                case 0: //do nothing
                        return;
            }
        }
    }
    private void SelArticleOP(POOBoard b){
        int res;
        while(true){
            System.out.printf("\n\n\n------------=%s=-----------------\n",b.boardName);
            System.out.print("#1. Create New Article.\n");
            System.out.print("#2. Browse Article.\n");
            System.out.print("#3. Delete Article.\n");
            System.out.print("#4. Modify article order.\n");
            System.out.print("#0. return to Top\n\n");
            System.out.print("Please Selcet option: ");
            res = getInteger();
            switch(res){
                case 1: AddNewArticle(b);
                        break;
                case 2: selArticleMenu(b);
                        break;
                case 3: DeleteArticle(b);
                        break;
                case 4: MoveArticleLoc(b);
                        break;
                case 0: //do nothing
                        return;
            }
        }
        
    }
    private void SelBoardMenu(){
        int selBoard = 0;
        while(true){
            System.out.print("\n\n\n------------=Board=List=-----------------\n");
            showBoard();
            System.out.printf("#0. return to Board Menu\n\n");
            System.out.printf("Please Selcet #Board: ");
            selBoard = getInteger();
            if(selBoard==0)   return;
            else if(selBoard>board_length)  continue;
            else{
                System.out.printf("Current Board is %s.\n",board[selBoard-1].boardName);
                SelArticleOP(board[selBoard-1]);
                break;
            }
        }
        
    }
    private void selFolderMenu(POODirectory d){
        int selBoard = 0;
        if(d.length()==0){
            System.out.printf("No Entry available.\n");
            return;
        }
        while(true){
            System.out.printf("\n\n\n------------=%s=-----------------\n",d.name);
            ShowCurrentContent();
            System.out.printf("#0. return to Top\n\n");
            System.out.printf("Please Selcet #Folder/Board: ");
            selBoard = getInteger();
            if(selBoard==0)   return;
            else if(selBoard > d.length())  continue;
            else{
                System.out.printf("Current Board is ");
                d.show(selBoard-1);
                int op = d.cd(selBoard-1);
                switch(op){
                    case 1: SelArticleOP(d.cd_board(selBoard-1));
                            break;
                    case 2: this.DirQueue[queueLen++] = d.cd_dir(selBoard-1);
                            selMyFolderOP();
                            this.queueLen--;
                            break;
                    case 3: //do nothing
                            continue;
                }
                break;
            }
        }

    }
    private void selArticleMenu(POOBoard b){
        int selBoard = 0;
        while(true){
            System.out.printf("\n\n\n------------=%s=-----------------\n",b.boardName);
            b.show();
            System.out.printf("#0. return to Board Menu\n\n");
            System.out.printf("Please Select #Article: ");
            selBoard = getInteger();
            if(selBoard==0)   return;
            else if(selBoard > b.length())  continue;
            else{
                System.out.printf("Current Article is ");
                b.Articles[selBoard-1].show(selBoard);
                ModifyArticleOP(b.Articles[selBoard-1]);
                break;
            }
        }
    }
    private void ModifyArticleOP(POOArticle a){
        int res;
        String tmp;
        while(true){
            System.out.printf("\n\n\n----score: %d------=%s=------by %s------\n",a.score,a.title,a.author);
            System.out.printf("#1. display article content.\n");
            System.out.printf("#2. push comment.\n");
            System.out.printf("#3. boo comment.\n");
            System.out.printf("#4. arrow comment.\n");
            System.out.printf("#0. return to Top\n\n");
            System.out.printf("Please Select #Operation: ");
            res = getInteger();
            if(res == 0) return;
            else if(res > 4) continue;
            else{
                switch(res){
                    case 1: a.showContent();
                            break;
                    case 2: System.out.printf("push: ");
                            a.pushComment(scan.nextLine(), userName, 1);
                            break;
                    case 3: System.out.printf("boo : ");
                            a.pushComment(scan.nextLine(), userName, -1);
                            break;
                    case 4: System.out.printf("->  : ");
                            a.pushComment(scan.nextLine(), userName, 0);
                            break;
                }
            }

        }
    }

    private int ModifyFolderOP(){
        int res;
        POODirectory tmp = DirQueue[queueLen-1];
        while(true){
            System.out.printf("\n\n\n------------=%s=-----------------\n",tmp.name);
            System.out.print("#1. Add new Board.\n");
            System.out.print("#2. Add new Folder.\n");
            System.out.print("#3. Add new split line.\n");
            System.out.print("#0. return to Top\n\n");
            System.out.print("Please Selcet option: ");
            res = getInteger();
            if(res >= 0 && res <= 3)  return res;
        }
    }
    /*Action functions*/
    public void CreateNewBoard(){
         System.out.print("Please enter board name: ");
         String newStr = scan.nextLine();
         if(newStr.length()!=0)
             CreateBoard(newStr);

    }
    public void BrowseBoard(){ 
        if(board_length==0){
            System.out.print("No boards available now.\n\n");
            return;
        }
        SelBoardMenu();
    }
    private static String[] buffer = new String[1024];
    public void AddNewArticle(POOBoard b){
        String Title,Author;
        System.out.printf("Please Enter Author: ");
        Author = scan.nextLine();
        System.out.printf("Please Enter Title: ");
        Title = scan.nextLine();
        System.out.printf("Please Paste the article below (Ctrl+x, line break to exit): \n");
        
        int length = 0;
        String tmp;
        while(true){
            tmp = scan.nextLine();
            if(tmp.length()!=0)
               if(((int)tmp.charAt(tmp.length()-1))==24)
                   break;
            buffer[length] = tmp;
            length++;
        }
        if(length!=0){
            POOArticle p = new POOArticle(b.length(),Author,Title,length,buffer,b.boardName);
            b.add(p);
        }
        
    }
    public void AddNewEntry(){
        System.out.printf("Current folder is %s.\n",DirQueue[queueLen-1].name);
        int res2 = ModifyFolderOP();
        if(res2 == 1){
            showBoard();
            System.out.print("Please enter a board number: ");
            int num = getInteger();
            if(num <= 0 || num > board_length)
                return;
            CreateBoardToFolder(num-1);
        }else if(res2 == 2){
            System.out.print("Please enter a folder name: ");
            CreateFolderToFolder(scan.nextLine());
        }else{
            CreateLineToFolder();
        }
    }
    public void DeleteCurrentBoard(){
        if(board_length==0){
            System.out.print("No borads available now.\n\n");
            return;
        }
        showBoard();
        System.out.print("Please enter a board number: ");
        int num = getInteger();
        if(num <= 0 || num > board_length)
            return;
        DeleteBoard(num-1);
    }
    public void DeleteEntry(){
        ShowCurrentContent();
        POODirectory tmp = DirQueue[queueLen - 1];
        System.out.print("Please enter a number: ");
        int num = num = getInteger();
        if(num <= 0 || num > tmp.length())
            return;
        tmp.del((short)(num-1));
    }
    public void DeleteArticle(POOBoard b){
        b.show();
        System.out.print("Please enter a number: ");
        int pos = getInteger();
        if(pos<= 0 || pos > b.count)
            return;
        b.del(pos-1);
    }
    public void MoveEntryLoc(POODirectory tmp){
        int src,dst;
        ShowCurrentContent();
        System.out.print("Please enter a source: ");
        src = getInteger();
        System.out.print("Please enter a destination:");
        dst = getInteger();
        if(src<= 0 || src > tmp.length() || dst<=0 || dst > tmp.length())
            return;
        tmp.move(src-1,dst-1);
    }
    public void MoveArticleLoc(POOBoard b){
        int src,dst;
        b.show();
        System.out.print("Please enter a source: ");
        src = getInteger();
        System.out.print("Please enter a destination:");
        dst = getInteger();
        if(src<= 0 || src > b.length() || dst<=0 || dst > b.length())
            return;
        b.move(src-1,dst-1);
    }
    /*util Function*/
    public boolean CreateBoard(String name){
        for(int i=0;i<board_length;i++)
            if(name.equals(board[i].boardName)){
                System.out.printf("Board name %s has already existed.\n",name);
                return false;
            }
        board[board_length] = new POOBoard(name);
        board_length++;
        return true;
    }
    public boolean DeleteBoard(int num){
        System.out.printf("Delete %s\n",board[num].boardName);
        for(int k=num+1;k<board_length;k++)
            board[k-1] = board[k];
        board_length--;
        return true;
    }
    public void showBoard(){
        for(int i=0;i<board_length;i++){
            System.out.printf("#%d. %s\n",i+1,board[i].boardName);
        }
        return;
    }
    public void CreateFolderToFolder(String name){
        if(name.length()==0)    return;
        POODirectory tmp = DirQueue[queueLen - 1];
        tmp.add((new POODirectory(name)));
    }
    public void CreateBoardToFolder(int num){
        POODirectory tmp = DirQueue[queueLen - 1];
        tmp.add(board[num]);
    }
    public void CreateLineToFolder(){
        POODirectory tmp = DirQueue[queueLen - 1];
        tmp.add_split();
    }
    public void ShowCurrentContent(){
        POODirectory tmp = DirQueue[queueLen - 1];
        tmp.show();
    }
    public void destructor(){
        for(int i=0;i<board_length;i++){
            board[i].del_self();
        }
    }
}
