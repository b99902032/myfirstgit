import java.io.*;

public class POOArticle{
    int digit;
    public int score;
    int NumcontentLine;
    int NumcommentLine;
    File file;
    public String title;
    public String author;
    
    static String[] content;
    static String[] comment;
    static int total_count = 0;
    static final int MEXVAL = 26;

    public POOArticle(int id, String name){
        this.digit = id;
        this.NumcontentLine = this.NumcommentLine = 0;

         /*using dummy article for UI*/
        if(id < 0)  return;
        /*read regular file from id*/
        this.file  = new File(name + "_" + Integer.toString(id) + ".POO" );
        if(!this.file.isFile()){
            id = -1;
            return;
        }
        try{
            RandomAccessFile access = new RandomAccessFile(this.file, "rw");
            this.title  = access.readLine();
            this.author = access.readLine();
        
            String[] arr = access.readLine().split("\\D+(?<![+-])");
            this.score = Integer.valueOf(arr[0]);
            this.NumcontentLine = Integer.valueOf(arr[1]);
            this.NumcommentLine = Integer.valueOf(arr[2]);
            access.close();

        }catch(IOException e){
            System.out.println("IOException:");
            e.printStackTrace();
        }
        total_count++;
        return;
    }

    public POOArticle(int id, String NewAuthor, String NewTopic, int d_length, String[] data, String name){
         /*Creating new Article*/
        this.digit   = id;
        this.author  = NewAuthor;
        this.title   = NewTopic;
        this.NumcontentLine = d_length;
        this.file  = new File(name + "_" + Integer.toString(id) + ".POO" );
        if(this.file.isFile()) return;
        try{
            this.file.createNewFile();
            RandomAccessFile access = new RandomAccessFile(this.file, "rw");
            String formatStr = "%03d %03d %03d\n";
            access.writeChars(this.title + "\n");
            access.writeChars(this.author + "\n");
            access.writeChars(String.format(formatStr,0,d_length,0));
            for(int i=0;i<d_length;i++)
                access.writeChars(data[i] + "\n");
            access.close();
        }catch(IOException e){
            System.out.println("IOException:");
            e.printStackTrace();
        }
        total_count++;
        return;
    }
    public void show(int num){
        System.out.print( "#." + num + "\t" + this.goLineN(1) + "\t" + this.goLineN(0) + "\n");
    }
    public void showContent(){
        String tmp;
        try{
            RandomAccessFile access = new RandomAccessFile(this.file, "r");
            for(int i=0;i<3;i++)
                access.readLine();
            for(int i=0;i<this.NumcontentLine+this.NumcommentLine;i++)
                System.out.print(access.readLine()+"\n");
             access.close();
        }catch(IOException e){
            System.out.println("IOException:");
            e.printStackTrace();
        }
        
    }
    public void pushComment(String s, String user, int value){
        if(s.length() > 26){
            s = s.substring(0,26);
        }
        this.NumcommentLine++;
        this.score += value;
        this.changeCommentStatus(s,user,value);

    }
    private String goLineN(int N){
        String tmp;
        try{
             RandomAccessFile access = new RandomAccessFile(this.file, "r");
             tmp = access.readLine();
             for(int i=1;i<=N;i++)
                 tmp = access.readLine();
             access.close();
             return tmp;
        }catch(IOException e){
            System.out.println("IOException:");
            e.printStackTrace();
        }
        return null;
    }
    private void changeCommentStatus(String s, String user, int value){
        String tmp;
        try{
            RandomAccessFile access = new RandomAccessFile(this.file, "rw");
            String formatStr = "%03d %03d %03d\n";
            for(int i=0;i<2;i++)
                access.readLine();
            access.writeChars(String.format(formatStr,this.NumcommentLine,this.NumcontentLine,this.score));
            if(s!=null){
                for(int i=0;i<this.NumcontentLine+this.NumcommentLine;i++)
                    tmp = access.readLine();
                if(value==0)
                    access.writeChars(user + " ->   : " + s + "\n");
                else if(value<0)
                    access.writeChars(user + " boo  : " + s + "\n");
                else
                    access.writeChars(user + " push : " + s + "\n");
            }
            access.close();
            return;
        }catch(IOException e){
            System.out.println("IOException:");
            e.printStackTrace();
        }
    }
    public void del(){
        this.file.delete();
    }
}
