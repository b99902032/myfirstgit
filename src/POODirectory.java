public class POODirectory{

    private short ListLen;
    private short BoardLen;
    private short DirLen;
    public POODirectory[] Dirlist;
    public POOBoard[] Boardlist;
    public short[][] Userlist;
    public String name;
    
    static final short DIRECTORY = 1;
    static final short BOARD = 2;
    static final short SPLIT_LINE = 3;

    public POODirectory(String s){
        this.Dirlist = new POODirectory[1024];
        this.Boardlist = new POOBoard[1024];
        this.Userlist = new short[1024][2];
        this.name = s;
        this.ListLen = 0;
        this.BoardLen = 0;
        this.DirLen = 0;
    }
    public void add(POOBoard b){
        this.Boardlist[this.BoardLen] = b;
        addList(this.BoardLen, BOARD);
        this.BoardLen++;
    }
    public void add(POODirectory d){
        this.Dirlist[this.DirLen] = d;
        addList(this.DirLen, DIRECTORY);
        this.DirLen++;
    }
    public void add_split(){
        addList((short)0, SPLIT_LINE);
    }
    public void del(short pos){
        if(pos > this.ListLen)   return;
        if(this.Userlist[pos][1] == SPLIT_LINE){
            delList(pos);
        }else if(this.Userlist[pos][1] == BOARD){
            delBoard(this.Userlist[pos][0]);
            delList(pos);
        }else{
            delBoard(this.Userlist[pos][0]);
            delList(pos);
        }
    }
    public void move(int src, int dst){
        if(src >= this.ListLen || dst >= this.ListLen)
            return;
        if(src == dst) return;
        if(src > dst){
            int tmp = src;
            src = dst;
            dst = tmp;
        }
        //System.out.printf("%d %d\n",src,dst);
        for(int i=src+1;i<=dst;i++){
        //System.out.printf("%d %d %d\n",i,src,dst);
            swap(i-1,i);
        }
    }
    public int length(){
        return this.ListLen;
    }
    public void show(){
        for(int i=0;i<this.ListLen;i++){
            show(i);
        }
    }
    public void show(int entry){
        int i = entry;
        int pos = this.Userlist[i][0];
        if(this.Userlist[i][1] == BOARD){
            System.out.print( "#" + (i+1) + " Board: " + this.Boardlist[pos].boardName + "\n");    
        }else if(this.Userlist[i][1] == DIRECTORY){
            System.out.print( "#" + (i+1) + " Dir  : " + this.Dirlist[pos].name + "\n");    
        }else{
            System.out.print( "#" + (i+1) + "------------------------------------\n");
        }

    }
    private void swap(int src, int dst){
        short tmp_1 = this.Userlist[src][0];
        short tmp_2 = this.Userlist[src][1];
        this.Userlist[src][0] = this.Userlist[dst][0];
        this.Userlist[src][1] = this.Userlist[dst][1];
        this.Userlist[dst][0] = tmp_1;
        this.Userlist[dst][1] = tmp_2;
    }
    public int cd(int entry){
        int i = entry;
        int pos = this.Userlist[i][0];
        if(this.Userlist[i][1] == BOARD){
            return 1;
        }else if(this.Userlist[i][1] == DIRECTORY){
            return 2;
        }else{
            return 3;
        }
    }
    public POOBoard cd_board(int entry){
        int i = entry;
        int pos = this.Userlist[i][0];
        if(this.Userlist[i][1] == BOARD){
            return this.Boardlist[pos];
        }
        return null;
    }
    public POODirectory cd_dir(int entry){
        int i = entry;
        int pos = this.Userlist[i][0];
        if(this.Userlist[i][1] == DIRECTORY){
            return this.Dirlist[pos];
        }
        return null;
    }
    private void addList(short pos, short type){
        this.Userlist[this.ListLen][0] = pos;
        this.Userlist[this.ListLen][1] = type;
        this.ListLen++;
    }

    private void delList(int pos){
        for(int i=pos+1;i<this.ListLen;i++){
            this.Userlist[i-1][0] = this.Userlist[i][0];
            this.Userlist[i-1][1] = this.Userlist[i][1];
        }
        this.ListLen--;
    }
    private void delBoard(int pos){
        for(int i=pos+1;i<this.BoardLen;i++){
            this.Boardlist[i-1] = this.Boardlist[i];
        }
        this.BoardLen--;
    }
    private void delDir(int pos){
        for(int i=pos+1;i<this.DirLen;i++){
            this.Dirlist[i-1] = this.Dirlist[i];
        }
        this.DirLen--;
    }
}

