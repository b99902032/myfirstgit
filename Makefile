GS = -g
JC = javac
CD = cd 
DIR = src/
.SUFFIXES: .java .class
.java.class: $(JC) $(JFLAGS) $*.java

CLASSES = \
          POOArticle.java \
          POOBoard.java \
          POODirectory.java \
          UI.java \
		  demo.java   

default: classes

classes:
	$(CD) $(DIR); $(JC) $(CLASSES)

run:
	$(CD) $(DIR); java demo; rm -rf *.POO;

clean:
	$(CD) $(DIR); $(RM) *.class
